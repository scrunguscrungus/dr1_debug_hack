#ifndef _UTIL_H
#define _UTIL_H

#include <windows.h>
#include "shared_def.h"



CStatModder* GetStatModder();

CGameManager* GetGameManager();

DWORD UTIL_AddressCalc(DWORD addr);


#endif