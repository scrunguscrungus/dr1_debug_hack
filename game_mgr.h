#ifndef _GAME_MGR_H
#define _GAME_MGR_H

#include <stdio.h>
#include "gameStat.h"
#include "statModder.h"


class CGameManager
{
	gameStat* m_gsStats[2048];

public:
	void RegisterStat(gameStat* gs);
	//void UnregisterStat(gameStat gs);

	//gameStat* GetAllStats() const;
	gameStat* GetStat(int stat);
	gameStat* GetStat(const char* statName);

	int GetStatNumber(const char* statName);
};



#endif