/////////////////////////////////////////////////////////////////////////
// Game stat class
//////////////////////////////////////////////////////////////////////////

#include "gameStat.h"
#include <stdio.h>

gameStat::gameStat(const char* statName, DWORD statAddress, gameStat_Type statType)
{
	strncpy_s(m_szStatName, statName, sizeof(m_szStatName));
	m_dStatAddress = statAddress;
}

char* gameStat::GetStatName()
{
	return m_szStatName;
}

DWORD gameStat::GetStatAddress()
{
	return m_dStatAddress;
}

gameStat_Type gameStat::GetStatType()
{
	return m_gstType;
}

