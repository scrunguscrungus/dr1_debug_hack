// dllmain.cpp : Defines the entry point for the DLL application.
#include <windows.h>
#include <stdio.h>
#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>
#include <float.h>
#include <io.h>
#include <fcntl.h>
#include <tchar.h>
#include <strsafe.h>
#include "psapi.h"
#include "stats.h"
#include "game_mgr.h"
#include "shared_def.h"
#include "util.h"
using namespace std;



//Stat displayer
//StatsDisplay *pStats = new StatsDisplay();

//HMODULE hDanganBase;






DWORD WINAPI Main_Think(LPVOID lpParam)
{
	while (true)
	{
		if ( GetStatModder() != NULL )
			GetStatModder()->StatModder_Main();
	}

	Sleep(200);
	return 0;
}
/*
DWORD WINAPI TestFunc(LPVOID lpParam)
{


	while (1) {
		//pStats->DrawStatsDisplay();

		/*
		if (GetAsyncKeyState(VK_NUMPAD1) & 1) {
				//WriteProcessMemory(bf2142, (void*)(0x05E2C88), &StandingOFF, 3, 0);
			float iPosX;
			DWORD readAddr = (DWORD)BaseAddr + dwPlPosX;
			printf("Reading Pos A from %08X\n", readAddr);
			int readPos = ReadProcessMemory(hDangan, (void*)readAddr, &iPosX, sizeof(iPosX), 0);
			if (!readPos)
			{
				printf( "Failed to read memory.\n" );
			}
			printf("Player Pos A: %3f\n", iPosX);
		}
		if (GetAsyncKeyState(VK_ADD) & 1) {
			//WriteProcessMemory(bf2142, (void*)(0x05E2C88), &StandingOFF, 3, 0);
			float iPosX;
			DWORD readAddr = (DWORD)BaseAddr + dwPlPosX;
			ReadProcessMemory(hDangan, (void*)readAddr, &iPosX, sizeof(iPosX), 0);
			iPosX += 1;
			printf("New Player Pos A: %3f\n", iPosX);
			WriteProcessMemory(hDangan, (void*)readAddr, &iPosX, sizeof(iPosX), 0);
			
		}
		if (GetAsyncKeyState(VK_SUBTRACT) & 1) {
			//WriteProcessMemory(bf2142, (void*)(0x05E2C88), &StandingOFF, 3, 0);
			float iPosX;
			DWORD readAddr = (DWORD)BaseAddr + dwPlPosX;
			ReadProcessMemory(hDangan, (void*)readAddr, &iPosX, sizeof(iPosX), 0);
			iPosX -= 1;
			WriteProcessMemory(hDangan, (void*)readAddr, &iPosX, sizeof(iPosX), 0);
			ReadProcessMemory(hDangan, (void*)readAddr, &iPosX, sizeof(iPosX), 0);
			printf("New Player Pos A: %3f\n", iPosX);

		}

			

	}

	Sleep(200);



	return 0;
}*/

void RegisterAllStats()
{
	//Player's (X??? Y???) position
	DWORD dwPlPosY = 0x0033CC80;
	//Z Position
	DWORD dwPlPosZ = 0x0033CC84;
	//Player's (X??? Y???) position
	DWORD dwPlPosX = 0x0033CC88;

	printf("Registering game stats...\n");
	gGameManager->RegisterStat( new gameStat( "Player Freeroam X", dwPlPosX, GS_TYPE_FLOAT ) );
	gGameManager->RegisterStat( new gameStat( "Player Freeroam Y", dwPlPosY, GS_TYPE_FLOAT ) );
	gGameManager->RegisterStat( new gameStat( "Player Freeroam Z", dwPlPosZ, GS_TYPE_FLOAT ) );
}

BOOL WINAPI DllMain(HINSTANCE hModule, DWORD dwAttached, LPVOID lpvReserved)
{
	
	HANDLE stdHandle;
	int hConsole;
	FILE* fp;

	//We're in!
	if (dwAttached == DLL_PROCESS_ATTACH) {

		//hDangan = GetCurrentProcess();
		//BaseAddr = (DWORD)GetModuleHandle(NULL);

		//Getting a handle on our executable
		hDangan = OpenProcess(PROCESS_ALL_ACCESS, false, GetCurrentProcessId());


		MODULEINFO mModuleInfo;

		//Find the module info for the executable we're injected into
		GetModuleInformation(GetCurrentProcess(), GetModuleHandle(NULL), &mModuleInfo, sizeof(mModuleInfo));
		//Find the base address
		BaseAddr = mModuleInfo.lpBaseOfDll;

		//Output console
		BOOL bConsole = AllocConsole();

		if (bConsole && hDangan)
		{
			char pszWindowTitle[MAX_PATH];
			char pszExecutableName[MAX_PATH];
			GetModuleFileNameExA(hDangan, NULL, pszExecutableName, MAX_PATH);

			sprintf_s(pszWindowTitle, "Jill's DR Debug Patch - Console");
			wchar_t wWindowTitle[MAX_PATH];
			mbstowcs(wWindowTitle, pszWindowTitle, strlen(pszWindowTitle) + 1);//Plus null

			SetConsoleTitle(wWindowTitle);
			
			//Route output to console
			stdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
			hConsole = _open_osfhandle((long)stdHandle, _O_TEXT);
			fp = _fdopen(hConsole, "w");

			freopen_s(&fp, "CONOUT$", "w", stdout);

			//Intro
			printf("Jill's DR debug patch: Successfully injected into Danganronpa at %08X!\n", BaseAddr);
			printf("Debug output enabled!\n");
			//Debug stuff
			//printf("Player Pos: %08X\n", dwPlPosX);
			RegisterAllStats();



			CreateThread(NULL, 0, &Main_Think, NULL, 0, NULL);
		}

		
	}
	return 1;
}