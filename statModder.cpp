#include <windows.h>
#include "statModder.h"
#include "game_mgr.h"
#include "util.h"

CStatModder::CStatModder()
{
	printf("StatModder constructor\n");
	SelectStat(0);
}

void CStatModder::StatModder_Main()
{
	if (GetAsyncKeyState(VK_NUMPAD4) & 1) {
		SelectStat(m_iCurrentStat + 1);
	}

	if (GetAsyncKeyState(VK_NUMPAD6) & 1) {
		SelectStat(m_iCurrentStat - 1);
	}

	if (GetAsyncKeyState(VK_ADD) & 1) {
		Stat_Add(m_iAmount);
	}

	if (GetAsyncKeyState(VK_SUBTRACT) & 1) {
		Stat_Subtract(m_iAmount);
	}
}

void CStatModder::SelectStat(int stat)
{
	if (!GetGameManager())
	{
		printf("Bad manager\n");
		return;
	}
		

	if (GetGameManager()->GetStat(stat) == NULL)
	{
		printf("CStatModder::SelectStat(): Attempted to select a NULL stat\n");
		return;
	}
	m_gsCurrentStat = GetGameManager()->GetStat(stat);
	m_gstCurrentStatType = m_gsCurrentStat->GetStatType();
	m_iCurrentStat = stat;
	printf("Stat Selected: %s\n", m_gsCurrentStat->GetStatName());
}


void CStatModder::SelectStat(const char* stat)
{
	if (!GetGameManager())
		return;
	if (GetGameManager()->GetStat(stat) == NULL)
	{
		printf("CStatModder::SelectStat(): Attempted to select a NULL stat\n");
		return;
	}
	m_gsCurrentStat = GetGameManager()->GetStat(stat);
	m_gstCurrentStatType = m_gsCurrentStat->GetStatType();
	m_iCurrentStat = GetGameManager()->GetStatNumber(stat);
	printf("Stat Selected: %s\n", m_gsCurrentStat->GetStatName());
}

void CStatModder::Stat_Add(int amt)
{
	int iStat;
	float fStat;
	bool bStat;

	DWORD readAddr = UTIL_AddressCalc(m_gsCurrentStat->GetStatAddress());

	switch (m_gstCurrentStatType)
	{
	case GS_TYPE_UNKNOWN:
		printf("Unknown stat type. Can't operate!\n");
		break;

	case GS_TYPE_INT:
		
		
		ReadProcessMemory(hDangan, (void*)readAddr, &iStat, sizeof(iStat), 0);
		iStat += amt;
		WriteProcessMemory(hDangan, (void*)readAddr, &iStat, sizeof(iStat), 0);
		printf("Updated Stat %s: %3f\n", m_gsCurrentStat->GetStatName(), iStat);
		//printf("Current stat: %3i\n", iStat);
		break;

	case GS_TYPE_FLOAT:
		
		ReadProcessMemory(hDangan, (void*)readAddr, &fStat, sizeof(fStat), 0);
		fStat += amt;
		WriteProcessMemory(hDangan, (void*)readAddr, &fStat, sizeof(fStat), 0);
		printf("Updated Stat %s: %3f\n", m_gsCurrentStat->GetStatName(), fStat);
		//printf("Current stat: %3i\n", iStat);
		break;

	case GS_TYPE_BOOL:
		
		ReadProcessMemory(hDangan, (void*)readAddr, &bStat, sizeof(bStat), 0);
		bStat = 1;
		WriteProcessMemory(hDangan, (void*)readAddr, &bStat, sizeof(bStat), 0);
		printf("Updated Stat %s: %3f\n", m_gsCurrentStat->GetStatName(), bStat);
		//printf("Current stat: %3i\n", iStat);
		break;
	}
}

void CStatModder::Stat_Subtract(int amt)
{
	int iStat;
	float fStat;
	bool bStat;

	DWORD readAddr = UTIL_AddressCalc(m_gsCurrentStat->GetStatAddress());

	switch (m_gstCurrentStatType)
	{
	case GS_TYPE_UNKNOWN:
		printf("Unknown stat type. Can't operate!\n");
		break;

	case GS_TYPE_INT:
		
		ReadProcessMemory(hDangan, (void*)readAddr, &iStat, sizeof(iStat), 0);
		iStat -= amt;
		WriteProcessMemory(hDangan, (void*)readAddr, &iStat, sizeof(iStat), 0);
		printf("Updated Stat %s: %3f\n", m_gsCurrentStat->GetStatName(), iStat);
		//printf("Current stat: %3i\n", iStat);
		break;

	case GS_TYPE_FLOAT:
		

		ReadProcessMemory(hDangan, (void*)readAddr, &fStat, sizeof(fStat), 0);
		fStat -= amt;
		WriteProcessMemory(hDangan, (void*)readAddr, &fStat, sizeof(fStat), 0);
		printf("Updated Stat %s: %3f\n", m_gsCurrentStat->GetStatName(), fStat);
		//printf("Current stat: %3i\n", iStat);
		break;

	case GS_TYPE_BOOL:
		

		ReadProcessMemory(hDangan, (void*)readAddr, &bStat, sizeof(bStat), 0);
		bStat = 0;
		WriteProcessMemory(hDangan, (void*)readAddr, &bStat, sizeof(bStat), 0);
		printf("Updated Stat %s: %3f\n", m_gsCurrentStat->GetStatName(), bStat);
		//printf("Current stat: %3i\n", iStat);
		break;
	}
}
