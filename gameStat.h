//////////////////////////////////////////////////////////////////////////
// Game stat class
//////////////////////////////////////////////////////////////////////////
#ifndef _GAME_STAT_H
#define _GAME_STAT_H

#include <windows.h>

enum gameStat_operation
{
	GS_OPERATION_ADD,
	GS_OPERATION_SUB
};

enum gameStat_Type
{
	GS_TYPE_UNKNOWN,
	GS_TYPE_INT,
	GS_TYPE_FLOAT,
	GS_TYPE_BOOL
};


class gameStat
{
	char m_szStatName[32];
	DWORD m_dStatAddress;
	gameStat_Type m_gstType;

public:
	gameStat(const char* statName, DWORD statAddress, gameStat_Type statType);

	char* GetStatName();
	DWORD GetStatAddress();
	gameStat_Type GetStatType();

	void EditSelectedStat(gameStat_operation operation);

	
};

#endif //_GAME_STAT_H