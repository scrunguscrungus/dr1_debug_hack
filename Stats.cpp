/*
-----------------------------------------
* Game hacking QTS ( Quickie Tip Series )
* no. 20 - Statistics ( Frames per second, Client size, Date & Time )
-----------------------------------------
* Author: SEGnosis      - GHAnon.net
* Thanks to:
* bitterbanana          - No known site
* Drunken Cheetah       - No known site
* fatboy88              - No known site
* Geek4Ever             - No known site
* learn_more            - www.uc-forum.com
* Novocaine             - http://ilsken.net/blog/?page_id=64
* Philly0494            - No known site
* Roverturbo            - www.uc-forum.com
* SilentKarma           - www.halocoders.com - offline
* Strife                - www.uc-forum.com
* Wieter20              - No known site
*/

#if 0
#include <stdio.h>
#include <time.h>
#include <d3dx9.h>
#include "stats.h"
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

void StatsDisplay::DrawStatsDisplay(void)
{
	//----------------------------------//
	//Client stats



	pDevice->GetViewport(&pViewPort);

	
	char screenResText[MAX_PATH];
	sprintf_s(screenResText, "Screen Width: %d Height: %d", pViewPort.Width, pViewPort.Height);
	Draw->Text(screenResText, 10, 10, centered, 0, false, RED(255), WHITE(0));


	//----------------------------------//
	// Date and time

	time_t rawtime;
	struct tm * timeinfo;

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	Draw->Text(asctime(timeinfo), 10, 30, centered, 0, false, RED(255), WHITE(0));


	//----------------------------------//
	// Frames per second

	static float fLastTime = GetTickCount(),
		fCurrentTime = GetTickCount();

	static float fFramesPerSecond = 0,
		fTotal = 0,
		fAverage = 0;


	fCurrentTime = GetTickCount();

	fTotal = (fFramesPerSecond + 0.1f) / ((fCurrentTime - fLastTime) / 1000);

	if (fCurrentTime - fLastTime > 5000)
	{
		fLastTime = fCurrentTime;
		fFramesPerSecond = 0;
		fAverage = fTotal;
	}

	char FPSText[MAX_PATH];
	sprintf_s(FPSText, "FPS: %f", (fTotal == 0 ? fAverage : fTotal));
	Draw->Text(FPSText, 10, 40, centered, 0, false, RED(255), WHITE(0));


	fFramesPerSecond++;
	

	//----------------------------------//
}
#endif