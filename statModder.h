#ifndef _STAT_MODDER_H
#define _STAT_MODDER_H

#include "gameStat.h"

class CStatModder
{
	gameStat* m_gsCurrentStat = NULL;
	int m_iCurrentStat = 0;
	gameStat_Type m_gstCurrentStatType = GS_TYPE_UNKNOWN;
	int m_iAmount = 1;


public:
	CStatModder();

	void StatModder_Main();

	void SelectStat(int stat);
	void SelectStat(const char* stat);

	void Stat_Add(int amt);
	void Stat_Subtract(int amt);
};


#endif // !_STAT_MODDER_h

