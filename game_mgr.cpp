#include "game_mgr.h"

void CGameManager::RegisterStat(gameStat* gs)
{
	for (int i = 0; i < sizeof(m_gsStats); i++)
	{
		if (m_gsStats[i]->GetStatName() == gs->GetStatName())
		{
			printf("CGameManager::RegisterStat(): Attempted to register >1 stat with name %s\n", gs->GetStatName());
			return;
		}
		if (m_gsStats[i]->GetStatAddress() == gs->GetStatAddress())
		{
			printf("CGameManager::RegisterStat(): Attempted to register >1 stat with address %08X\n", gs->GetStatAddress());
			return;
		}
		if (m_gsStats[i] == NULL)
		{
			m_gsStats[i] = gs;
		}
	}
}

gameStat* CGameManager::GetStat(int stat)
{
	return m_gsStats[stat];
}

gameStat* CGameManager::GetStat(const char* statName)
{
	for (int i = 0; i < sizeof(m_gsStats); i++)
	{
		if (m_gsStats[i]->GetStatName() == statName)
		{
			return m_gsStats[i];
		}
	}
}

int CGameManager::GetStatNumber(const char* statName)
{
	for (int i = 0; i < sizeof(m_gsStats); i++)
	{
		if (m_gsStats[i]->GetStatName() == statName)
		{
			return i;
		}
	}
}

